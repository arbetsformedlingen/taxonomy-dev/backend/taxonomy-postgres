(ns jobtech.taxonomy-postgres
  (:require [datahike.api :as d]
            [datahike-jdbc.core]
            [wanderung.core :as w]
            )
  (:gen-class))


(def nippy-cfg-v21
  {:wanderung/type :nippy
   :filename       "resources/taxonomy-v21.nippy"})


;; podman run --detach --publish 5432:5432 --env POSTGRES_DB=taxonomy --env POSTGRES_USER=admin --env POSTGRES_PASSWORD=password postgres:alpine


(def postgres-cfg {
                  :wanderung/type :datahike
                   :store {
                          :id "taxonomy-postgres"
                          :backend :jdbc
                          :dbtype "postgresql"
                          :host "localhost"
                          :port 5432
                          :user "admin"
                          :password "password"
                          :dbname "taxonomy"}
                   :schema-flexibility :write
                   :keep-history? true ;; it was like this before :keep-history :true
                   :attribute-refs? true
                   :name "taxonomy"
                   :index :datahike.index/persistent-set
                   }
  )


#_(defn create-taxonomy-db []
  (d/create-database postgres-cfg))

(defn migrate-nippy-to-postgres []
  (w/migrate nippy-cfg-v21 postgres-cfg))
